package net.schnegg.eval.kinesis.producer;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.AmazonKinesisClientBuilder;
import com.amazonaws.services.kinesis.model.CreateStreamRequest;
import com.amazonaws.services.kinesis.model.DescribeStreamRequest;
import com.amazonaws.services.kinesis.model.DescribeStreamResult;
import com.amazonaws.services.kinesis.producer.KinesisProducer;
import com.amazonaws.services.kinesis.producer.KinesisProducerConfiguration;
import net.schnegg.eval.proto.Person;
import org.assertj.core.api.Fail;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static com.amazonaws.SDKGlobalConfiguration.AWS_CBOR_DISABLE_SYSTEM_PROPERTY;
import static com.amazonaws.SDKGlobalConfiguration.DISABLE_CERT_CHECKING_SYSTEM_PROPERTY;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.KINESIS;

@Testcontainers
class TestLocalstack {

    public static final String STREAM_NAME = "myStreamName";

    DockerImageName localstackImage = DockerImageName.parse("localstack/localstack:0.11.3");

    @Container
    public LocalStackContainer localstack = new LocalStackContainer(localstackImage)
            .withServices(KINESIS);

    public void createTestStream() throws InterruptedException {

        System.setProperty(AWS_CBOR_DISABLE_SYSTEM_PROPERTY, "true");
        System.setProperty(DISABLE_CERT_CHECKING_SYSTEM_PROPERTY, "true");

        ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setProtocol(Protocol.HTTP);

        AmazonKinesis client = AmazonKinesisClientBuilder
                .standard()
                .withEndpointConfiguration(localstack.getEndpointConfiguration(KINESIS))
                .withCredentials(localstack.getDefaultCredentialsProvider())
                .withClientConfiguration(clientConfiguration)
                .build();

        CreateStreamRequest createStreamRequest = new CreateStreamRequest();
        createStreamRequest.setStreamName(STREAM_NAME);
        createStreamRequest.setShardCount(1);

        client.createStream(createStreamRequest);

        DescribeStreamRequest describeStreamRequest = new DescribeStreamRequest();
        describeStreamRequest.setStreamName( STREAM_NAME );
        Thread.sleep(1000);
        DescribeStreamResult describeStreamResponse = client.describeStream( describeStreamRequest );
        String streamStatus = describeStreamResponse.getStreamDescription().getStreamStatus();
        System.out.println("Stream status: " + streamStatus);
    }

    public KinesisProducer getKinesisProducer() {
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(localstack.getAccessKey(), localstack.getSecretKey());
        KinesisProducerConfiguration config = new KinesisProducerConfiguration();
        config.setRegion(localstack.getRegion());
        config.setCredentialsProvider(new AWSStaticCredentialsProvider(awsCreds));
        config.setMaxConnections(1);
        config.setRequestTimeout(6000); // 6 seconds
        config.setRecordMaxBufferedTime(5000); // 5 seconds
        config.setKinesisEndpoint(localstack.getEndpointOverride(KINESIS).getHost());
        config.setKinesisPort(localstack.getEndpointOverride(KINESIS).getPort());
        KinesisProducer kinesisProducer = new KinesisProducer(config);
        return kinesisProducer;
    }

    @Test
    void testLocalStackKinesis() throws InterruptedException {

        assertTrue(localstack.isRunning());

        createTestStream();

        ProducerService service = new ProducerServiceImpl(getKinesisProducer());
        Person johnDoe = Person.newBuilder().setName("John Doe").setEmail("john@doe.com").build();
        try {
            service.putDataIntoKinesis(johnDoe);
        } catch (Exception e) {
            Fail.fail("writing to kinesis failed: ", e);
        } finally {
            //service.stop();
            localstack.stop();
        }
    }
}