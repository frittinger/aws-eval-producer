package net.schnegg.eval.kinesis.producer;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.kinesis.producer.KinesisProducer;
import com.amazonaws.services.kinesis.producer.KinesisProducerConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AWSConfiguration {

    @Value(value = "${aws.region}")
    private String awsRegion;
    @Value(value = "${aws.access_key}")
    private String awsAccessKey;
    @Value(value = "${aws.secret_key}")
    private String awsSecretKey;

    @Bean
    public KinesisProducer getKinesisProducer() {

        BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsAccessKey, awsSecretKey);

        KinesisProducerConfiguration config = new KinesisProducerConfiguration();
        config.setRegion(awsRegion);
        config.setCredentialsProvider(new AWSStaticCredentialsProvider(awsCreds));
        config.setMaxConnections(1);
        config.setRequestTimeout(6000); // 6 seconds
        config.setRecordMaxBufferedTime(5000); // 5 seconds

        KinesisProducer kinesisProducer = new KinesisProducer(config);

        return kinesisProducer;
    }
}
