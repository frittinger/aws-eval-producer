package net.schnegg.eval.kinesis.producer;

import net.schnegg.eval.proto.Person;

public interface ProducerService {

    public void putDataIntoKinesis(Person person) throws Exception;
    public void stop();

}
