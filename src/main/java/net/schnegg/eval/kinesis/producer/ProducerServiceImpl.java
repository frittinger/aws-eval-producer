package net.schnegg.eval.kinesis.producer;

import com.amazonaws.services.kinesis.producer.Attempt;
import com.amazonaws.services.kinesis.producer.KinesisProducer;
import com.amazonaws.services.kinesis.producer.UserRecordFailedException;
import com.amazonaws.services.kinesis.producer.UserRecordResult;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import lombok.extern.slf4j.Slf4j;
import net.schnegg.eval.proto.Person;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.stereotype.Service;

import java.nio.ByteBuffer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@Service
public class ProducerServiceImpl implements ProducerService {

    private static final String TIMESTAMP_AS_PARTITION_KEY = Long.toString(System.currentTimeMillis());
    // The number of records that have finished (either successfully put, or failed)
    final AtomicLong completed = new AtomicLong(0);

    //@Value(value = "${aws.stream_name}")
    private String streamName = "myStreamName";

    private final KinesisProducer kinesisProducer;

    public ProducerServiceImpl(KinesisProducer kinesisProducer) {
        this.kinesisProducer = kinesisProducer;
    }

    @Override
    public void putDataIntoKinesis(Person person) throws Exception {

        FutureCallback<UserRecordResult> myCallback = new FutureCallback<>() {

            @Override
            public void onSuccess(@Nullable UserRecordResult result) {

                long totalTime = result.getAttempts().stream()
                        .mapToLong(a -> a.getDelay() + a.getDuration()).sum();

                log.info("Data writing success. Total time taken to write data = {}", totalTime);

                completed.getAndIncrement();
            }

            @Override
            public void onFailure(Throwable t) {

                // If we see any failures, we will log them.
                int attempts = ((UserRecordFailedException) t).getResult().getAttempts().size() - 1;
                if (t instanceof UserRecordFailedException) {
                    Attempt last =
                            ((UserRecordFailedException) t).getResult().getAttempts().get(attempts);
                    if (attempts > 1) {
                        Attempt previous = ((UserRecordFailedException) t).getResult().getAttempts()
                                .get(attempts - 1);
                        log.error(String.format(
                                "Failed to put record - %s : %s. Previous failure - %s : %s",
                                last.getErrorCode(), last.getErrorMessage(),
                                previous.getErrorCode(), previous.getErrorMessage()));
                    } else {
                        log.error(String.format("Failed to put record - %s : %s.",
                                last.getErrorCode(), last.getErrorMessage()));
                    }

                }
                log.error("Exception during put", t);
            }

        };

        final ExecutorService callbackThreadPool = Executors.newCachedThreadPool();

        // wait until unfinished records are processed
        while (kinesisProducer.getOutstandingRecordsCount() > 1e4) {
            Thread.sleep(1);
        }

        // write data to Kinesis stream
        ListenableFuture<UserRecordResult> f =
                kinesisProducer.addUserRecord(streamName, TIMESTAMP_AS_PARTITION_KEY, ByteBuffer.wrap(person.toByteArray()));

        Futures.addCallback(f, myCallback, callbackThreadPool);

    }

    @Override
    public void stop() {
        if (kinesisProducer != null) {
            kinesisProducer.flushSync();
            kinesisProducer.destroy();
        }
    }
}