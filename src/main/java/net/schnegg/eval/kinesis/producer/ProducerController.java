package net.schnegg.eval.kinesis.producer;

import lombok.extern.slf4j.Slf4j;
import net.schnegg.eval.proto.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/api")
public class ProducerController {

    @Autowired
    private ProducerService producerService;

    @PostMapping(value = "/stream")
    public ResponseEntity<String> putIntoKinesis(@RequestBody SimplePerson person) {

        Person personProto = person.getProto();
        try {
            producerService.putDataIntoKinesis(personProto);
        } catch (Exception e) {
            log.error("Error putting person on stream: {}", person, e);
        }

        return ResponseEntity.ok("Saved data into Kinessis sucessfully!");
    }

}